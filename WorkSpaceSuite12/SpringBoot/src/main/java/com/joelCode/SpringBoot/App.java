package com.joelCode.SpringBoot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.joelCode.beans.Mundo;

/**
 * Hello world!
 *
 */
public class App{
	 public static void main( String[] args ) {
		 ApplicationContext appContext = new
		ClassPathXmlApplicationContext("com/joelCode/xml/beans.xml");
		 Mundo m = (Mundo)appContext.getBean("mundo");
	 }
}
