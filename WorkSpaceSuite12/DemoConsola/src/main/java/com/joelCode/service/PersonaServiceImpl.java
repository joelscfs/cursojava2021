package com.joelCode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.joelCode.repository.IPersona;

public class PersonaServiceImpl implements IPersonaService {
	@Autowired
	@Qualifier("persona1")
	IPersona repo;
	@Override
	public void registrarHandler(String pNombre) {
		repo.registrar(pNombre);

	}

}
