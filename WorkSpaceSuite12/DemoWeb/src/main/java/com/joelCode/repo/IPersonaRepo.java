package com.joelCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.joelCode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
