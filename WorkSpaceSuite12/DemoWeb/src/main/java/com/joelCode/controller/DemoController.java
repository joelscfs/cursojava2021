package com.joelCode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.joelCode.model.Persona;
import com.joelCode.repo.IPersonaRepo;

@Controller
public class DemoController {
	@Autowired
	private IPersonaRepo repo;
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required= false, defaultValue="World") String name, Model model) {
		Persona per = new Persona(1, "Joel Sebastian");
		repo.save(per);
		
		model.addAttribute("name", name);
		return "greeting";
	}
}
