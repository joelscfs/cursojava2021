package es.com.manpower.notas.util.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.com.manpower.notas.util.ConnectionManager;

class ConnectionManagerTest {
	//Creamos un objeto de la clase Connection
	Connection con;

	@BeforeEach
	public void setUp() throws Exception {
		//1-Especificar el driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		//2-Crear la conexi�n ---> Es una interfaz
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "sistema", "sistema");	
		//Hace la conexi�n
		ConnectionManager.conectar();
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		con = null;
		ConnectionManager.desconectar();
	}

	@Test
	void testConectar() {
		try {
			ConnectionManager.conectar();
			//Conprueba que la conexion aun es v�lida
			assertTrue(ConnectionManager.getConnection().isValid(1000));
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testDesconectar() {
		try {
			ConnectionManager.desconectar();
			//Con este metodo comprobamos que esta cerrado
			assertTrue(ConnectionManager.getConnection().isClosed());
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testGetConnection() throws SQLException {
		assertFalse(ConnectionManager.getConnection().isClosed());
		
	}


}
