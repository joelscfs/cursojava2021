package es.com.manpower.notas.modelo;

import java.util.Objects;

public class PracticaResuelta implements Model {

	private int codigo;
	private float nota;
	private String observacion;
	
	
	public PracticaResuelta() {}

	public PracticaResuelta(int codigo, float nota, String observacion) {
		super();
		this.codigo = codigo;
		this.nota = nota;
		this.observacion = observacion;
	}

	public int getCodigo() {return codigo;}
	public void setCodigo(int codigo) {this.codigo = codigo;}

	public float getNota() {return nota;}
	public void setNota(float nota) {this.nota = nota;}

	public String getObservacion() {return observacion;}
	public void setObservacion(String observacion) {this.observacion = observacion;}

	
//	public boolean equals(Object obj) {
//		return obj instanceof PracticaResuelta &&
//				((PracticaResuelta) obj).getCodigo() == this.codigo &&
//				((PracticaResuelta) obj).getNota() == this.nota;
//	}
//	public int hashCode() {
//		return codigo + (int)nota;
//	}
//	public String toString() {
//		StringBuilder sb = new StringBuilder("codigo=");
//		sb.append(this.codigo);
//		sb.append(", nota=");
//		sb.append(this.nota);
//		sb.append(", observacion=");
//		sb.append(this.observacion);
//		return sb.toString();
//	}
	
	
	
}