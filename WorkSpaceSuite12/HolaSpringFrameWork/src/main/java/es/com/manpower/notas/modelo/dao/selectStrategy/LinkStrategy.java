package es.com.manpower.notas.modelo.dao.selectStrategy;

public class LinkStrategy extends SelectStrategy {

	public LinkStrategy() {	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder();
		
		if (this.tengoWhere) {
			sb.append(" and alu_linkgit like '%");
			sb.append(alumno.getLinkArepositorio());
			sb.append("%'");
		} else {
			sb.append(" where alu_linkgit like '%");
			sb.append(alumno.getLinkArepositorio());
			sb.append("\'");
			this.tengoWhere = true;
		}
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		return alumno.getLinkArepositorio() != null &&
				!alumno.getLinkArepositorio().isEmpty();
	}

}
