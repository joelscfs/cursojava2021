package es.com.manpower.notas.util.test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.com.manpower.notas.util.DateUtil;

class DateUtilTest {
	Date fecha;

	@BeforeEach
	void setUp() throws Exception {
		Calendar cal = Calendar.getInstance();
		//Asignamos la fecha
		cal .set(1972, Calendar.NOVEMBER, 17);
		fecha = cal.getTime();
	}

	@AfterEach
	void tearDown() throws Exception {
		fecha = null;
	}

	@Test
	void testGetAnio() {
		//Testeamos el m�todo de la clase que hemos creado (DateUtil)
		assertEquals(1972, DateUtil.getAnio(fecha));
		//Muestro por pantalla como se ve la fecha
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		System.out.println("Fecha ---> " + fecha);
		System.out.println("Calendar ---> " + cal);
		//Creo el objeto SimpleDateFormat ---> Se le indica el formato a utilizar
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		System.out.println("Calendar simplificado ---> " + sdf.format(fecha));
	}

}
