package patrones.factory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	private static List<PiedraPapelTijeraFactory> elementos;
	protected String nombre;
	protected int numero;
	protected String descripcionResultado;
	
	public PiedraPapelTijeraFactory() {
			
	}
	
	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public static PiedraPapelTijeraFactory getInstance(int pNumero) {
		//El padre conoce a todos los hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		//Recorro todos los hijos y cuando coincide lo devuelve
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNumero)) {
				return piedraPapelTijeraFactory;
			}
		}
		return null;
	}
	
	public abstract boolean isMe(int pNumero);
	public abstract int comparar(PiedraPapelTijeraFactory pptf);
}
