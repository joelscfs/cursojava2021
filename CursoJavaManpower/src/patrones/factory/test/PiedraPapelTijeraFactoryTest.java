package patrones.factory.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import patrones.factory.Papel;
import patrones.factory.Piedra;
import patrones.factory.PiedraPapelTijeraFactory;
import patrones.factory.Tijera;

class PiedraPapelTijeraFactoryTest {
	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;

	@BeforeEach
	void setUp() throws Exception {
		piedra= new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@AfterEach
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
	}

	@Test
	void testComparaPiedraPierdePapel() {
		//Comprobamos el entero que nos devuelve el metodo comparar
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("Piedra perdio con papel", piedra.getDescripcionResultado());
	}
	
	@Test
	void testGetInstancePiedra() {
		//Si el metodo devuelve una instancia de la clase Piedra esta bien
		assertTrue(PiedraPapelTijeraFactory.getInstance(0) instanceof Piedra);
	}
	
	@Test
	void testGetInstancePapel() {
		//Si el metodo devuelve una instancia de la clase Papel esta bien
		assertTrue(PiedraPapelTijeraFactory.getInstance(1) instanceof Papel);
	}
	
	@Test
	void testGetInstanceTijera() {
		//Si el metodo devuelve una instancia de la clase Tijera esta bien
		assertTrue(PiedraPapelTijeraFactory.getInstance(2) instanceof Tijera);
	}

}
