package patrones.factory;

public class Piedra extends PiedraPapelTijeraFactory{
	
	

	public Piedra() {
		numero = 0;
		nombre = "piedra";
		descripcionResultado="";
	}
	


	@Override
	public boolean isMe(int pNumero) {	
		return pNumero == 0;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pptf) {
		int resultado = 0;
		//Al ser protected puedo acceder a la variable del padre sin metodos
		switch(pptf.numero) {
		case 0: //Piedra
			resultado = 0; //Empata
			descripcionResultado = "Empatamos seguimos siendo amigos";
			break;
		case 1: //Papel
			resultado = -1; //Pierde
			descripcionResultado = "Piedra perdio con " + pptf.getNombre();
			break;
		case 2: //Tijera
			resultado = 1; //Gana
			descripcionResultado = "Piedra le gano a " + pptf.getNombre();
			break;
		default:
			break;
		}		
		return resultado;
	}



}
