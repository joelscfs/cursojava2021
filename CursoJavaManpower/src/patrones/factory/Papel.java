package patrones.factory;

public class Papel  extends PiedraPapelTijeraFactory{

	public Papel() {
		numero = 1;
		nombre = "papel";
	}

	@Override
	public boolean isMe(int pNumero) {
		return pNumero== 1;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pptf) {
		int resultado = 0;
		//Al ser protected puedo acceder a la variable del padre sin metodos
		switch(pptf.numero) {
		case 0: //Piedra
			resultado = 1; //Gana
			descripcionResultado = "Papel gano con " + pptf.getNombre();
			break;
		case 1: //Papel
			resultado = 0; //Empata
			descripcionResultado = "Empatamos seguimos siendo amigos";
			break;
		case 2: //Tijera
			resultado = -1; //Pierde
			descripcionResultado = "Papel perdio con " + pptf.getNombre();
			break;
		default:
			break;
		}		
		return resultado;
	}

}
