package practica5;

public class Practica5Ejercicio6 {

	public static void main(String[] args) {
		String cadena = "aadas4ddsa";
		boolean numeros = false;
		//Hago un bucle que comprueba que cada numero no esta en ninguna parte de la cadena
		for (int i = 0; i < 10; i++) {
			if(cadena.contains(i+"")) {
				numeros = true;
			}
		}
		//Compruebo la condicion y muestro el resultado
		if (numeros == true) {
			System.out.println("La cadena " + cadena + " tiene numeros");
		} else {
			System.out.println("La cadena " + cadena + " no tiene numeros");
		}
	}

}
