package practica5;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla_String {

	private JFrame frame;
	private JTextField txtTexto;
	private JLabel lblMayusculas;
	private JLabel lblO_2;
	private JLabel lblMinusculas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_String window = new Pantalla_String();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_String() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 683, 503);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEtiqueta = new JLabel("MANEJO DE STRING");
		lblEtiqueta.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		lblEtiqueta.setBounds(217, 48, 236, 25);
		frame.getContentPane().add(lblEtiqueta);
		
		JLabel lblIngresaTexto = new JLabel("Ingrese un texto: ");
		lblIngresaTexto.setFont(new Font("Trebuchet MS", Font.ITALIC, 16));
		lblIngresaTexto.setBounds(110, 145, 128, 19);
		frame.getContentPane().add(lblIngresaTexto);
		
		txtTexto = new JTextField();
		txtTexto.setFont(new Font("Trebuchet MS", Font.ITALIC, 16));
		txtTexto.setBounds(254, 142, 199, 25);
		frame.getContentPane().add(txtTexto);
		txtTexto.setColumns(10);
		
		JLabel lblMayus = new JLabel("MAYUSCULAS");
		lblMayus.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		lblMayus.setBounds(166, 215, 118, 24);
		frame.getContentPane().add(lblMayus);
		
		JLabel lblMinus = new JLabel("MINUSCULAS");
		lblMinus.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		lblMinus.setBounds(165, 268, 118, 24);
		frame.getContentPane().add(lblMinus);
		
		JLabel lblO = new JLabel("O ----> 2");
		lblO.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		lblO.setBounds(165, 324, 118, 24);
		frame.getContentPane().add(lblO);
		
		lblMayusculas = new JLabel("");
		lblMayusculas.setBackground(Color.CYAN);
		lblMayusculas.setOpaque(true);
		lblMayusculas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMayusculas.setBounds(309, 214, 152, 25);
		frame.getContentPane().add(lblMayusculas);
		
		lblMinusculas = new JLabel("");
		lblMinusculas.setOpaque(true);
		lblMinusculas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMinusculas.setBackground(Color.CYAN);
		lblMinusculas.setBounds(309, 268, 152, 25);
		frame.getContentPane().add(lblMinusculas);
		
		lblO_2 = new JLabel("");
		lblO_2.setOpaque(true);
		lblO_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblO_2.setBackground(Color.CYAN);
		lblO_2.setBounds(309, 324, 152, 25);
		frame.getContentPane().add(lblO_2);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Obtengo el texto y lo transformo a mayusculas y minusculas
				lblMayusculas.setText(txtTexto.getText().toUpperCase());
				lblMinusculas.setText(txtTexto.getText().toLowerCase());
				//cambiamos la letra o por el 2
				lblO_2.setText(txtTexto.getText().toLowerCase().replace('o', '2'));
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnCalcular.setBounds(242, 396, 119, 29);
		frame.getContentPane().add(btnCalcular);
	}
}
