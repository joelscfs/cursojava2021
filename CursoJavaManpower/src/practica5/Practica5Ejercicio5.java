package practica5;

public class Practica5Ejercicio5 {

	public static void main(String[] args) {
		String texto = "gcasas1972@gmail.com";
		//Con la funcion indexof consigo la posicion como si fuera un array de caracteres (Empieza en 0)
		int posicion = texto.indexOf("@") + 1;
		//Creo array para usar la funcion split
		String[] partes;
		//Declaro el caracter que va a separar la cadena
		partes = texto.split("@");
		//Asigno cada parte al array que he creado
		String primeraParte = partes[0];
		String segundaParte = partes[1];
		System.out.println("Primera parte: " + primeraParte);
		System.out.println("Segunda parte: " + segundaParte);
		System.out.println("El caracter @ ocupa la posicion: " + posicion);

	}

}
