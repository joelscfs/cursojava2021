package practica5;

public class Practica5Ejercicio4 {

	public static void main(String[] args) {
		String texto = "esto es una prueba de la clase String";
		int vocales = 0;
		int consonantes = 0;
		//Convierto a minusculas
		texto = texto.toLowerCase();
		for (int i = 0; i < texto.length(); i++) {
			//Recorro la cadena letra por letra
			char letra = texto.charAt(i);
			if(letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
				vocales++;
			} else if ((letra >= 'a' && letra <= 'z')) {
				consonantes++;
			}
		}
		//Muestro el resultado por pantalla
		System.out.println("El texto: " + texto + "\n" + "Tiene " + vocales + " vocales y " + consonantes + " consonantes");

	}

}
