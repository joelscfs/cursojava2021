package stringUtil;

public class StringUtil {

	public static boolean containsDobleSpace(String str) {
		int cont = 0;
		for (int i = 0; i < str.length(); i++) {
			if(str.contains("  ")) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean containsNumber(String str) {
		for (int i = 0; i < 10; i++) {
			if(str.contains(i+"")) {
				return true;
			}
		}
		return false;
	}
}
