package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio16 {

	public static void main(String[] args) {	
		int numTabla = 0;
		Scanner sc = new Scanner(System.in);	
		do {
			System.out.println("Indique n�mero del que quiera obtener su tabla: ");
			numTabla = sc.nextInt();
		} while (numTabla < 0 || numTabla > 10);
		System.out.println("--------------Tabla del " + numTabla + "--------------");		
		for (int i = 0; i <= 10; i++) {
			System.out.println(numTabla + " x " + i + " ---> " +numTabla * i);
		}	
		sc.close();
	}
}
