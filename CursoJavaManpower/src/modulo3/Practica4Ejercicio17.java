package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio17 {

	public static void main(String[] args) {
		int numTabla = 0;
		int sumaNumPares = 0;
		Scanner sc = new Scanner(System.in);	
		do {
			System.out.println("Indique n�mero del que quiera obtener su tabla: ");
			numTabla = sc.nextInt();
		} while (numTabla < 0 || numTabla > 10);
		System.out.println("--------------Tabla del " + numTabla + "--------------");		
		for (int i = 0; i <= 10; i++) {
			System.out.println(numTabla + " x " + i + " ---> " +numTabla * i);
			//Suma de los pares con if
			/*
			 * if(i % 2 == 0)
			{
				sumaNumPares += numTabla * i;
			}
			 */
		}	
		//Suma de los pares con algoritmo
			for (int i = 0; i <= 10; i+=2) {
				sumaNumPares += numTabla * i;
			}
			System.out.println("La suma de los numeros pares es ---> " + sumaNumPares);
		sc.close();
	}
}
