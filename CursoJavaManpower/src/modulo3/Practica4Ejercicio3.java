package modulo3;

public class Practica4Ejercicio3 {

	public static void main(String[] args) {
		/*
		 * Realizar un programa que permita determinar la cantidad de d�as que tiene el mes informando 
		el nombre del mismo acompa�ado de la cantidad de d�as que le corresponden. Recuerde que 30
		d�as tiene noviembre con abril junio y setiembre de 28 solo hay uno y los dem�s 31.
		 */
		int mes = 8;
		int numero;
        String cadenaMes;
        switch (mes) {
            case 1:  
            	cadenaMes = "Enero";
            	numero = 31;
            	System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 2:  
            	cadenaMes = "Febrero";
            	numero = 28;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 3:  
            	cadenaMes = "Marzo";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 4:  
            	cadenaMes = "Abril";
            	numero = 30;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 5:  
            	cadenaMes = "Mayo";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 6:  
            	cadenaMes = "Junio";
            	numero = 30;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 7:  
            	cadenaMes = "Julio";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 8:  
            	cadenaMes = "Agosto";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 9:  
            	cadenaMes = "Septiembre";
            	numero = 30;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 10: 
            	cadenaMes = "Octubre";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 11: 
            	cadenaMes = "Noviembre";
            	numero = 30;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            case 12:
            	cadenaMes = "Diciembre";
            	numero = 31;
                System.out.println("El mes " + cadenaMes + " tiene " + numero + " dias");
                break;
            default: 
            	cadenaMes = "No hay tantos meses";
                     break;
        }
	}

}
