package modulo3;

import java.util.Random;

public class Practica4Ejercicio20 {

	public static void main(String[] args) {
		final int NUM_MAX = 10;
		Random random = new Random();
		int aux = 0;
		//Establezco n�meros fuera de rango para que no los tenga en cuenta
		int numMin = 101;
		int numMax = 0;
		int contador = 0;
		while(contador < NUM_MAX){
			//Numeros random entre 1 y 100
			aux = random.nextInt(100) + 1;
			System.out.println("N�mero ---> " + aux);
			if(aux >= numMax){
				numMax = aux;
			}
			if(aux <= numMin){
				numMin = aux;
			}
			contador++;
		}
		System.out.println("N�mero m�nimo ---> " + numMin + ",N�mero m�xmimo ---> " + numMax);
	}
}
