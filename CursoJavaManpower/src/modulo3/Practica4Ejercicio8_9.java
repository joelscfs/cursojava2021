package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio8_9 {

	public static void main(String[] args) {
		//Asigno un numero a cada opci�n para identificarlo
		int piedra = 1;
		int papel = 2;
		int tijera = 3;
		String[] nombresEleccion = {"Piedra", "Papel", "Tijera"};
		//Pido al usuario que escoja una opcion
		Scanner sc = new Scanner(System.in);
		System.out.print("Jugador 1 elija su opcion 1-Piedra, 2-Papel, 3-Tijera");
		int jugador1 = sc.nextInt();
		System.out.print("Jugador 1 elija su opcion 1-Piedra, 2-Papel, 3-Tijera");
		int jugador2 = sc.nextInt();
		//Cierro el Scanner
		sc.close();
		//Comparo las jugadas con los operadores
		if (jugador1 == piedra && jugador2 == tijera || jugador1 == papel && jugador2 == piedra || jugador1 == tijera && jugador2 == papel) {
			System.out.println("Gana jugador1 -> " + nombresEleccion[jugador1-1] + " gana a " + nombresEleccion[jugador2 -1]);
		} else if(jugador1 == jugador2){
			System.out.println("Empate.");
		} else {
			System.out.println("Gana jugador2 -> " + nombresEleccion[jugador2-1] + " gana a " + nombresEleccion[jugador1 -1]);
		}
	}
}
