package modulo3;

import java.util.Random;

public class Practica4Ejercicio19 {

	public static void main(String[] args) {	
		final int NUM_MAX = 10;
		Random random = new Random();
		int aux = 0;
		int total = 0;
		int contador = 0;
		while(contador < NUM_MAX){
			//Numeros random entre 1 y 100
			aux = random.nextInt(100) + 1;
			System.out.println("N�mero ---> " + aux);
			total += aux;
			contador++;
		}
		System.out.println("\nTotal ---> " + total + ", Media ---> " + total / contador);
	}

}
