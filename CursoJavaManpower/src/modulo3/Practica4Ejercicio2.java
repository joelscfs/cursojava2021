package modulo3;

public class Practica4Ejercicio2 {

	public static void main(String[] args) {
		/*
		 * 2. Realizar un programa que permita identificar si un n�mero es par o impar, el mismo deber� 
			estar guardado en una variable de tipo int.
		 */
		int numero = 4;
		if (numero % 2 == 0) {
			System.out.println("El numero "+ numero + " es par");
		} else {
			System.out.println("El numero "+ numero + " es impar");
		}
	}

}
