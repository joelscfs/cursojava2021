package modulo3;

public class Practica4Ejercicio14 {

	public static void main(String[] args) {
		String categoria = "a";
		switch(categoria) {
		case "a":
			System.out.println("Hijo");
			break;
		case "b":
			System.out.println("Padres");
			break;
		case "c":
			System.out.println("Abuelos");
			break;
		default:
			System.out.println("Fallo");
			break;		
		}
	}
}
