package modulo3;

public class Practica4Ejercicio6 {

	public static void main(String[] args) {
		/*
		 * 6. Realizar un sistema que permita determinar a trav�s de una variable de tipo int correspondiente
			al curso al que pertenece seg�n el siguiente criterio
			a. 0 debe mostrarse en pantalla el texto �jard�n de infantes�.
			b. 1 y 6 se mostrara en pantalla el texto �primaria�.
			c. 7 y 12 se mostrara el texto �secundaria�.
		 */
		int num = 1;
		switch(num) {
		case 0:
			System.out.println("Jardin de infantes");
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			System.out.println("Primaria");
			break;
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
			System.out.println("Secundaria");
			break;
		default:
			System.out.println("Siga participando.");
			break;
		
		}

	}

}
