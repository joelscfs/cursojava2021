package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio12 {
	public static void main(String[] args) {	
		//Pido al usuario que seleccione un n�mero
		System.out.print("Seleccione un n�mero: ");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();	
		//Cierro el Scanner
		sc.close();
		//Mediante if y el operador and compruebo las condiciones
		if(numero >=1 && numero <= 12){
			System.out.println("Primera docena de numeros");
		} else if(numero >=13 && numero <= 24){
			System.out.println("Segunda docena de numeros");
		} else if(numero >=25 && numero <= 36){
			System.out.println("Tercera docena de numeros");
		} else {
			System.out.println("Nmero fuera de rango.");
		}
	}
}
