package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio10 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int resultado = 0;
		//Pido los numeros por pantalla
		System.out.print("Primer n�mero: ");
		int numero1 = sc.nextInt();	
		System.out.print("Segundo n�mero: ");
		int numero2 = sc.nextInt();	
		System.out.print("Tercer n�mero: ");
		int numero3 = sc.nextInt();
		//Cierro el Scanner
		sc.close();
		//Compruebo con el operador and
		if (numero1 > numero2 && numero1 > numero3){
			resultado = numero1;
		} else if (numero2 > numero1 && numero2 > numero3){
			resultado = numero2;
		} else {
			resultado = numero3;
		}
		//Muestro el resultado
		System.out.println("El n�mero ms alto de la selecci�n es ---> " + resultado);
	}
}
