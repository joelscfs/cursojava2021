package modulo3;

public class Practica4Ejercicio5 {

	public static void main(String[] args) {
		/*
		 *  Realizar un programa que permita identificar con una variable de tipo entero el puesto que 
			ocupa un torneo, existen 3 posiciones que son premiadas para los cuales deber� mostrar el 
			siguiente mensaje en pantalla:
			a. El primero obtiene la medalla de oro.
			b. El segundo obtiene la medalla de plata.
			c. Y el tercero obtiene la medalla de bronce.
			d. Y para el resto siga participando.
		 */
		int puesto = 1;
		switch(puesto) {
		case 1:
			System.out.println("El primero obtiene la medalla de oro.");
			break;
		case 2:
			System.out.println("El segundo obtiene la medalla de plata.");
			break;
		case 3:
			System.out.println("El tercero obtiene la medalla de bronce.");
			break;
		default:
			System.out.println("Siga participando.");
			break;
		
		}

	}

}
