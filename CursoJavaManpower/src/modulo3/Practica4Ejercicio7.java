package modulo3;

public class Practica4Ejercicio7 {

	public static void main(String[] args) {
		/*
		 *  Realizar un programa que permita la definición de 3 variables de tipo entera e imprimir la mayor
			de todas.
		 */
		int n1 = 3;
		int n2 = 5;
		int n3 = 9;
		
		if (n1 > n2) {
            if (n1 > n3) {
                System.out.println("El mayor es: " + n1);                                             
            } else {
                System.out.println("el mayor es: " + n3);     
            }
        } else if (n2 > n3) {
            System.out.println("el mayor es: " + n2);
        } else {
            System.out.println("el mayor es: " + n3);
        }
	}

}
