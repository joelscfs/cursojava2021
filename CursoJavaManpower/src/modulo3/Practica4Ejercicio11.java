package modulo3;

import java.util.Scanner;

public class Practica4Ejercicio11 {
	public static void main(String[] args) {
		//Declaro las variables de las vocales
		char a = 'a';
		char e = 'e';
		char i = 'i';
		char o = 'o';
		char u = 'u';
		//Abro el scanner para solcitar información al usuario
		Scanner sc = new Scanner(System.in);
		System.out.print("Indique una letra");
		//Pido la letra ---> utilizao charAt
		char letraUsuario = sc.nextLine().charAt(0);
		//Cierro el Scanner
		sc.close();
		//Utilizo el operador or para comparar las condiciones
		if(letraUsuario == a || letraUsuario == e || letraUsuario == i || letraUsuario == o || letraUsuario == u){
			System.out.println("Se ha introducido una vocal");
		} else {
			System.out.println("Se ha introducido una consonante");
		}
	}
}
