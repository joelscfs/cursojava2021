package modulo3;

import java.util.Iterator;
import java.util.Scanner;

public class EjemploIf {
	public static void main(String[] args) {
		int tabla = 0;
		do {
			System.out.println("Ingresa la tabla a mostrar menor igual a 10 y mayor que 0: ");
			Scanner sc = new Scanner(System.in);
			tabla = sc.nextInt();
		
		} while (tabla < 0 || tabla > 10);
		
		System.out.println("tabla del " + tabla);
		
		for (int i = 0; i < 11; i++) {
			int mult = tabla*i;
			System.out.println(i + " X " + tabla +" = " + mult);
		}
		
	}
}
