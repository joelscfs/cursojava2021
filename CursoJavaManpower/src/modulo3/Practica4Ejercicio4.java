package modulo3;

public class Practica4Ejercicio4 {

	public static void main(String[] args) {
		/*
		 * Realizar un programa que permita ingresar una categor�a las cuales pueden ser �a�, �b� o �c�, 
			a. Si corresponde a esta categor�a mostrar en pantalla la palabra �hijo�
			b. Si corresponde a esta categor�a mostrar en pantalla la palabra �padres�
			c. Si corresponde a esta categor�a mostrar en pantalla la palabra �abuelos�.
		 */
		String categoria = "a";
		switch(categoria) {
		case "a":
			System.out.println("Hijo");
			break;
		case "b":
			System.out.println("Padres");
			break;
		case "c":
			System.out.println("Abuelos");
			break;
		default:
			System.out.println("Fallo");
			break;
		
		}

	}

}
