package modulo2;

public class Ejercicio1 {
	public static void main(String[] args) {
		byte bMin = 127;
		short s = bMin;
		int i = 1357;
		//Necesita casteo, nos hacemos responsables si hay algun tipo de fallo
		float nro1 = 0;
		int nro2 = 3;
		float f = nro1/0;
		double raiz = Math.sqrt(-1);
		byte b = (byte)i;
		int nroCualquiera = 3;
		System.out.println("Numero cualquiera = " + ++nroCualquiera);
		System.out.println("raiz = " + raiz);
		System.out.println("f = " + f);
		System.out.println("bMin = " + bMin);
		System.out.println("i = " + i);
		System.out.println("b = " + b);
	}
}
