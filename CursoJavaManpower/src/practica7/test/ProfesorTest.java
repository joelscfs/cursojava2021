package practica7.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import practica7.Profesor;

public class ProfesorTest {
	
	List <Profesor> listaProfesores;
	Profesor profEjemplo;
	
	@BeforeEach
	public void setUp() throws Exception {
		profEjemplo = new Profesor("Casas","Gabriel","111");		
		listaProfesores = new ArrayList<Profesor>();
		listaProfesores.add(new Profesor("Casas","Gabriel","111"));
		listaProfesores.add(new Profesor("Rodriguez","Jorge","222"));
		listaProfesores.add(new Profesor("Lopez","Luis","333"));
	}

	@AfterEach
	public void tearDown() throws Exception {
		listaProfesores = null;
		profEjemplo = null;
	}

	@Test
	public void testHashCodeProfesor() {
		assertTrue(listaProfesores.contains(profEjemplo));
	}

	@Test
	public void testEqualsProfesor() {
		assertTrue(listaProfesores.contains(profEjemplo));
		Profesor prueba = new Profesor("Hernandez","Jorgito","444");
		assertFalse(profEjemplo.equals(prueba));
	}

	@Test
	public void testProfesorContructor() {
		assertEquals("Gabriel",profEjemplo.getNombre());
		profEjemplo.setIosfa("989");
		assertEquals("989",profEjemplo.getIosfa());
	}
	
}
