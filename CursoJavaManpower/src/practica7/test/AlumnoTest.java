package practica7.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import practica7.Alumno;

public class AlumnoTest {
	
	List <Alumno> listaAlumnos;
	Alumno alumnoPrueba;
	
	@BeforeEach
	public void setUp() throws Exception {
		alumnoPrueba = new Alumno("Sebastián","Joel",222);
		listaAlumnos = new ArrayList<Alumno>();
		listaAlumnos.add(new Alumno("hgasd","Monsef",111));
		listaAlumnos.add(new Alumno("Sebastián","Joel",222));
		listaAlumnos.add(new Alumno("Jimenez","Guillermo",333));
	
	}

	@AfterEach
	public void tearDown() throws Exception {
		listaAlumnos = null;
		alumnoPrueba = null;
	}

	@Test
	public void testHashCode() {
		assertTrue(listaAlumnos.contains(alumnoPrueba));
	}

	@Test
	public void testEqualsObject() {
		assertTrue(listaAlumnos.contains(alumnoPrueba));
		Alumno alumnoPrueba2 = new Alumno("Riojo","Luis",999);
		assertFalse(alumnoPrueba.equals(alumnoPrueba2));
	}

	@Test
	public void testAlumno() {
		assertEquals("Joel",alumnoPrueba.getNombre());
		alumnoPrueba.setLegajo(222);
		assertEquals(222,alumnoPrueba.getLegajo());
	}
}
