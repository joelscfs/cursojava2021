package es.com.manpower.notas.modelo.dao.selectStrategy;

public class ApellidoStrategy extends SelectStrategy {

	public ApellidoStrategy() { }

	@Override
	public String getCondicion() {
		
		StringBuilder sb = new StringBuilder();
		
		if (this.tengoWhere) {
			sb.append(" and alu_apellido like '%");
			sb.append(alumno.getApellido());
			sb.append("%'");
		} else {
			sb.append(" where alu_apellido like '%");
			sb.append(alumno.getApellido());
			sb.append("\'");
			this.tengoWhere = true;
		}
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		this.tengoWhere = alumno.getNombre() != null &&
				!alumno.getNombre().isEmpty();
		
		return alumno.getApellido() != null &&
				!alumno.getApellido().isEmpty();
	}

}
