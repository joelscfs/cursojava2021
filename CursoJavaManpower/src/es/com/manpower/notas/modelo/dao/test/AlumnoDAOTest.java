package es.com.manpower.notas.modelo.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.Model;
import es.com.manpower.notas.modelo.dao.AlumnoDAO;
import es.com.manpower.notas.modelo.dao.DAO;
import es.com.manpower.notas.util.ConnectionManager;

public class AlumnoDAOTest {
	
	//Defino el objeto que utilizare en las pruebas
	DAO alumnoDao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();	
	    Statement consulta= con.createStatement();
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
	    Statement consulta= con.createStatement();
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		alumnoDao = new AlumnoDAO();
	}

	@After
	public void tearDown() throws Exception {
		alumnoDao = null;
	}

	@Test
	public void testAgregar() {
		try {
			alumnoDao.agregar(new Alumno(0, "Gabriel_test", "Casas_test", "Estudios_test", "Repo_test"));
			//Comprobamos que el objeto se ha agregado ley�ndolo de la BD
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("select ALU_NOMBRE from alumnos where ALU_NOMBRE = 'Gabriel_test'");
			//Movemos el puntero para que apunte al objeto que queremos
			rs.next();
			assertEquals("Gabriel_test", rs.getString("ALU_NOMBRE"));
		} catch (ClassNotFoundException | SQLException e) {
			//Si se produce el error lo pinto de rojo
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		try {
			//1-Hace el select de Marina para saber su clave primaria
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("select ALU_ID from alumnos where ALU_NOMBRE = 'Marina_test'");
			rs.next();
			
			//2-Creo un objeto con todos los datos modificados ---> la clave la saco del ResultSet
			Alumno alu = new Alumno(rs.getInt("ALU_ID"), 
									"MarinaModif_test", 
									"Calvo Pere�aModif_test", 
									"Grado en F�sicaModif_test", 
									"RepoModif");
				//Llamo al metodo modificar con el objeto con los datos modificados como par�metro
			alumnoDao.modificar(alu);
			
			//3-Leo la BD para comprobar los datos de la modificaci�n
			StringBuilder sql = new StringBuilder("select ALU_ID, ALU_NOMBRE, ALU_APELLIDO, ALU_ESTUDIOS, ALU_LINKGIT");
			sql.append(" from alumnos");
			sql.append(" where ALU_NOMBRE = 'MarinaModif_test'");	
			rs = stm.executeQuery(sql.toString());
				//Movemos el puntero para que apunte al objeto que queremos
			rs.next();
			Alumno aluLeido = new Alumno(rs.getInt("ALU_ID"),
										rs.getString("ALU_NOMBRE"),
										rs.getString("ALU_APELLIDO"),
										rs.getString("ALU_ESTUDIOS"),
										rs.getString("ALU_LINKGIT"));
			
			assertEquals(alu.getCodigo(), aluLeido.getCodigo());
			assertEquals(alu.getNombre(), aluLeido.getNombre());
			assertEquals(alu.getApellido(), aluLeido.getApellido());
			assertEquals(alu.getEstudios(), aluLeido.getEstudios());
			assertEquals(alu.getLinkArepositorio(), aluLeido.getLinkArepositorio());
		} catch (ClassNotFoundException | SQLException e) {
			//Si se produce el error lo pinto de rojo
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		try {
			//1-Leo los datos del objeto que quiero eliminar
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
				//Obtengo el ID del objeto a eliminar
			ResultSet rs = stm.executeQuery("select ALU_ID from alumnos where ALU_NOMBRE = 'Monsef_test'");
			rs.next();
			//2-Elimino el objeto
			Alumno alu = new Alumno(rs.getInt("ALU_ID")); //Constructor con solo el c�digo
			alumnoDao.eliminar(alu);
			//3-Compruebo que la eliminaci�n se ha producido
			rs = stm.executeQuery("select ALU_NOMBRE from alumnos where ALU_NOMBRE = 'Monsef_test'");		
			assertFalse(rs.next());
		} catch (ClassNotFoundException | SQLException e) {
			//Si se produce el error lo pinto de rojo
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testLeerPorCodigo() {
		
		try {
			//1-Leo los datos del objeto que quiero comprobar que existe
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
				//Obtengo el ID del objeto a eliminar
			ResultSet rs = stm.executeQuery("select ALU_ID from alumnos where ALU_NOMBRE = 'Aar�n_test'");
			rs.next();
			//2-Ejecuto el m�todo leer y lo guardo en una lista
			Alumno alu = new Alumno(rs.getInt("ALU_ID"));
			List<Model>listaAlumnos = alumnoDao.leer(alu);
				//Casteo a Alumno y obtengo los datos con los getter
			assertEquals("Aar�n_test", ((Alumno)listaAlumnos.get(0)).getNombre());
			assertEquals("S�nchez S�nchez_test", ((Alumno)listaAlumnos.get(0)).getApellido());
			assertEquals("Desarrollo de Aplicaciones Multiplataforma_test", ((Alumno)listaAlumnos.get(0)).getEstudios());
			assertEquals("https://github.com/Pashinian/CursoJava2021.git_test", ((Alumno)listaAlumnos.get(0)).getLinkArepositorio());
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}

}
