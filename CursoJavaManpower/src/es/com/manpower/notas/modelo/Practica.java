package es.com.manpower.notas.modelo;

import java.util.Objects;

public class Practica implements Model {

	private int codigo;
	private String nombre;
	
	public Practica() {
		
	}
	
	public Practica(int codigo) {
		super();
		this.codigo = codigo;
	}

	public Practica(int codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public int getCodigo() {return codigo;}
	public void setCodigo(int codigo) {this.codigo = codigo;}

	public String getNombre() {return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public boolean equals(Object obj) {
		return obj instanceof Alumno &&
				((Practica) obj).getCodigo() == codigo &&
				((Practica) obj).getNombre().equals(nombre);
	}
	public int hashCode() {
		return nombre.hashCode() + codigo;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(this.codigo);
		sb.append(", nombre=");
		sb.append(this.nombre);
		return sb.toString();
	}
	

}