package es.com.manpower.notas.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	
	//Todas las excepciones en el modelo y las pruebas se lanzan
	private static Connection connection;
	public static void conectar() throws ClassNotFoundException, SQLException{
		//1-Especificar el driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		//2-Crear la conexi�n ---> Es una interfaz
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "sistema", "sistema");
	}
	
	public static void desconectar() throws SQLException {
		connection.close();
	}
	
	public static Connection getConnection() {
		return connection;
	}

}
