package objetos;

import objetos.exceptions.CuentaException;

public class CuentaTest {

	public static void main(String[] args) throws CuentaException {
		//Crea un objeto
		Cuenta c1 = new CajaDeAhorro();
		Cuenta c2 = new CajaDeAhorro(20, 2000,0.2f);
		
		System.out.println("c1 = " + c1);
		System.out.println("c2 = " + c2);
		//Mostramos la informaci�n por pantalla
		System.out.println("Se cre� el objeto 1 " + c1.getNumero());
		System.out.println("Su saldo es: " + c1.getSaldo());
		
		System.out.println("\nSe cre� el objeto 1 " + c2.getNumero());
		System.out.println("Su saldo es: " + c2.getSaldo());

		//Probamos los m�todos
		System.out.println("\nA c1 le voy a acreditar 100");
		c1.acreditar(100); //M�todo acreditar
		System.out.println("El nuevo saldo de c1 es: " + c1.getSaldo());
		
		System.out.println("\nA c1 le voy a debitar 80");
		c1.debitar(80); //M�todo debitar
		System.out.println("El nuevo saldo de c1 es: " + c1.getSaldo());
	}

}
