package objetos.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import objetos.CajaDeAhorro;
import objetos.Cuenta;
import objetos.CuentaCorriente;
import objetos.exceptions.CuentaException;

class CuentaTest {
	Cuenta cajaDeAhorroVacio;
	Cuenta cajaDeAhorroLleno;
	Cuenta cc1Vacio;
	
	//Colecciones
	List<Cuenta> lstCuentas; //List es una interfaz ---> Permite duplicados
	Set<Cuenta> setCuentas; //No permite duplicados
	
	@BeforeEach
	void setUp() throws Exception {
		//Upcast
		cajaDeAhorroVacio = new CajaDeAhorro();
		cajaDeAhorroLleno = new CajaDeAhorro(20, 2000,0.2f);
		cc1Vacio = new CuentaCorriente(); //Un nuevo objeto siempre se crea con la instrucción new
		
		//Creamos una lista heterogenea
		
		lstCuentas = new ArrayList<Cuenta>();
		lstCuentas.add(new CajaDeAhorro());
		lstCuentas.add(new CuentaCorriente());
		
		lstCuentas.add(new CajaDeAhorro(1,10,0.1f));
		lstCuentas.add(new CajaDeAhorro(2,20,0.2f));
		lstCuentas.add(new CajaDeAhorro(3,30,0.3f));
		
		lstCuentas.add(new CuentaCorriente(10, 1000, 100));
		lstCuentas.add(new CuentaCorriente(20, 2000, 200));
		lstCuentas.add(new CuentaCorriente(30, 3000, 300));
		
		
		setCuentas = new HashSet<Cuenta>();
		setCuentas.add(new CajaDeAhorro());
		setCuentas.add(new CuentaCorriente());
		
		setCuentas.add(new CajaDeAhorro(1,10,0.1f));
		setCuentas.add(new CajaDeAhorro(2,20,0.2f));
		setCuentas.add(new CajaDeAhorro(3,30,0.3f));
		
		setCuentas.add(new CuentaCorriente(10, 1000, 100));
		setCuentas.add(new CuentaCorriente(20, 2000, 200));
		setCuentas.add(new CuentaCorriente(30, 3000, 300));
		
		//Se crean 19 cuentas

	}

	@AfterEach
	void tearDown() throws Exception {
		cajaDeAhorroVacio = null;
		cajaDeAhorroLleno = null;
		cc1Vacio = null;	
		lstCuentas = null;
		setCuentas = null;
		Cuenta.setCantidadDeCuentas(0);
	}
	
	//Probamos el metodo contanis en los dos tipos de listas
	@Test
	public void listaEqualsContainsFALSE() {
		Cuenta cPrueba = new CajaDeAhorro();
		cPrueba.acreditar(100);
		assertFalse(lstCuentas.contains(cPrueba));
	}
	
	@Test
	public void listaEqualsContainsTRUE() {
		Cuenta cPrueba = new CajaDeAhorro();
		assertTrue(lstCuentas.contains(cPrueba));
	}
	
	//Probamos el metodo add en los dos tipos de listas
	@Test
	public void listaAdd() {
		Cuenta cPrueba = new CajaDeAhorro();
		assertTrue(lstCuentas.add(cPrueba));
		System.out.println("lstCuentas " + lstCuentas); //Lo muestra con el toString DE CADA TIPO DE OBJETO
	}
	
	@Test
	public void setAdd() {
		Cuenta cPrueba = new CajaDeAhorro();
		assertFalse(setCuentas.add(cPrueba));
		System.out.println("setCuentas " + setCuentas); //Lo muestra con el toString DE CADA TIPO DE OBJETO
	}
	
	@Test
	public void testCajaDeAhorroEqualsVERDADERO() {
		Cuenta cajaDeAhorro = new CajaDeAhorro();
		assertTrue(cajaDeAhorroVacio.equals(cajaDeAhorro));
	}
	
	@Test
	public void testCajaDeAhorroEqualsFALSO() {
		Cuenta cajaDeAhorro = new CajaDeAhorro();
		cajaDeAhorro.acreditar(100);
		assertFalse(cajaDeAhorroVacio.equals(cajaDeAhorro));
	}
	
	@Test
	public void testCantidadDeCuentas() {
		assertEquals(19, Cuenta.getCantidadDeCuentas());
	}

	@Test
	void testCuenta() {
		//pruebo el constructor vacio
		assertEquals(10, cajaDeAhorroVacio.getNumero());
		assertEquals(1000.0f, cajaDeAhorroVacio.getSaldo(), 0.01);
	}

	@Test
	void testCuentaIntFloat() {
		assertEquals(20, cajaDeAhorroLleno.getNumero());
		assertEquals(2000.0f, cajaDeAhorroLleno.getSaldo(), 0.01);
	}

	@Test
	void testAcreditar() {
		cajaDeAhorroVacio.acreditar(100.53f);
		assertEquals(1100.53f, cajaDeAhorroVacio.getSaldo(), 0.01);
	}

	@Test
	void testDebitarCajaDeAhorroALCANZA() {
		try {
			cajaDeAhorroVacio.debitar(100);
			assertEquals(900.0f, cajaDeAhorroVacio.getSaldo(), 0.01);
		} catch (CuentaException e) {
			assertTrue(false); //Lo muestra de rojo (Fallo)
			e.printStackTrace();
		}
	}
	
	@Test
	void testDebitaCuentaCorrienteALCANZA() {
		//Saldo 1000 ---> descubierto 150
		try {
			cc1Vacio.debitar(200);
			assertEquals(800.0f, cc1Vacio.getSaldo(), 0.01);
		} catch (CuentaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testDebitarCajaDeAhorro_NO_ALCANZA() {
		try {
			cajaDeAhorroVacio.debitar(1100);
			assertTrue(false);
		} catch (CuentaException e) {
			//El mensaje tiene que ser el mismo que en el throws
			assertEquals("No tienes suficiente dinero", e.getMessage()); 
			e.printStackTrace();
		}
	}
	
	@Test
	void testDebitaCuentaCorriente_ALCANZA_CON_DESCUBIERTO() {
		try {
			cc1Vacio.debitar(1100);
			assertEquals(-100.0f, cc1Vacio.getSaldo(), 0.01);
		} catch (CuentaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testDebitaCuentaCorriente_NO_ALCANZA_CON_DESCUBIERTO() {
		try {
			cc1Vacio.debitar(1200);
			assertTrue(false);
		} catch (CuentaException e) {
			assertEquals("No tienes suficiente dinero", e.getMessage());
			e.printStackTrace();
		}
	}
	

}
