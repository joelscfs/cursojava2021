package objetos;

import objetos.exceptions.CuentaException;

/**
 * 
 * @author sebas
 * Esta clase guarda los datos de una cuenta banacaria
 *
 */
public abstract class Cuenta {
	
	//Atributos ---> encapsulamiento
	private static int cantidadDeCuentas;
	private int numero;
	private float saldo;
	
	//Constructores
	public Cuenta() {
		numero = 10;
		saldo = 1000;
		cantidadDeCuentas++;
	}
	
	public Cuenta(int pNumero, float pSaldo) {
		numero = pNumero;
		saldo = pSaldo;
		cantidadDeCuentas++;
	}
	
	//Getters & Setters
	
	/**
	 * Este m�todo asigna el numero de cuenta
	 * @param pNumero es el par�metro con el numero de cuenta
	 */
	public void setNumero(int pNumero) {
		numero = pNumero;
	}
	
	public int getNumero() {
		return numero;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	
	public static int getCantidadDeCuentas() {
		return cantidadDeCuentas;
	}

	public static void setCantidadDeCuentas(int cantidadDeCuentas) {
		Cuenta.cantidadDeCuentas = cantidadDeCuentas;
	}

	//Metodos de negocio
	public void acreditar(float pMonto) {
		saldo += pMonto;  //saldo = saldo + pMonto;
		
	}
	
	public abstract void debitar(float pMonto) throws CuentaException;
	
	public boolean equals(Object obj) { //Esta sobreescrito ---> Clase Object
		//Establezco las reglas de negocio a mi elecci�n
		boolean bln = false;
		//Si el objeto es una cuenta...
		if (obj instanceof Cuenta) {
			//Downcast ---> Manual ---> Se decide a que tipo de objeto se hace el casteo
			Cuenta cue = (Cuenta) obj;
			bln = this.numero == cue.getNumero() && this.saldo == cue.getSaldo();
		}
		return bln;
	}
	
	public int hashCode() {
		return numero + (int)saldo;
	}
	
	public String toString() {
		return "Numero = " + numero + ", saldo = " + saldo;
	}
	
}
