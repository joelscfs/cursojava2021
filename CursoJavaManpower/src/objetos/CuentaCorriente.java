package objetos;

import java.util.Objects;

import objetos.exceptions.CuentaException;

public class CuentaCorriente extends Cuenta {
	//Atributos
	private float descubierto;
	
	public CuentaCorriente() {
		super(15, 1000);
		descubierto = 150;
	}

	public CuentaCorriente(int pNumero, float pSaldo, float pDescubierto) {
		super(pNumero, pSaldo);
		descubierto = pDescubierto;
	}

	//Método abstracto sobreescrito
	@Override
	public void debitar(float pMonto) throws CuentaException{
		if(pMonto < (getSaldo() + descubierto)) {
			setSaldo(getSaldo() - pMonto);
		} else {
			throw new CuentaException("No tienes suficiente dinero");
		}

	}

	
	//Getters & Setters
	public float getDescubierto() {
		return descubierto;
	}

	public void setDescubierto(float descubierto) {
		this.descubierto = descubierto;
	}
	
	//Metodos sobreescritos ---> Mayor detalle y especialización
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(descubierto);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuentaCorriente other = (CuentaCorriente) obj;
		return Float.floatToIntBits(descubierto) == Float.floatToIntBits(other.descubierto);
	}

	@Override
	public String toString() {
		return "\nCuenta Corriente " + super.toString() + ", descubierto = " + descubierto;
	}
	
	
}
