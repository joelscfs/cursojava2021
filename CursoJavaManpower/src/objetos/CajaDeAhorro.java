package objetos;

import objetos.exceptions.CuentaException;

public class CajaDeAhorro extends Cuenta {
	
	//Atributos
	private float interes;
	
	//Constructores
	public CajaDeAhorro() {
		super(10, 1000);
		interes = 0.1f;
	}

	public CajaDeAhorro(int pNumero, float pSaldo, float pInteres) {
		super(pNumero, pSaldo);
		interes = pInteres;
	}

	
	//Getters & Setters
	public float getInteres() {
		return interes;
	}

	public void setInteres(float interes) {
		this.interes = interes;
	}
	
	//M�todos
	//Se detalla mejor el metodo abstracto por eso la flechita azul
	public void debitar(float pMonto) throws CuentaException {
		//No se puede debitar mas de la cantidad que se tiene
		if (pMonto <= getSaldo()) {
			setSaldo(getSaldo() - pMonto);
		} else {
			throw new CuentaException("No tienes suficiente dinero");
		}
	}
	
	//Metodos sobreescritos ---> Mayor detalle
	public boolean equals (Object obj) {
		return obj instanceof CajaDeAhorro && super.equals(obj) && interes == ((CajaDeAhorro)obj).getInteres();
	}
	
	public int hashCode() {
		return super.hashCode() + (int)(interes * 100);
	}
	
	public String toString() {
		return "\nCaja de Ahorro " + super.toString() + ", interes = " + interes;
	}
}
