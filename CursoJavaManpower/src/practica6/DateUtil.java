package practica6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static int getAnio(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return cal.get(Calendar.YEAR);
	}
	
	public static int getMes(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return cal.get(Calendar.MONTH);
	}
	
	public static int getDia(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	public static boolean isFinDeSemana(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		int dia = cal.get(Calendar.DAY_OF_WEEK);
		if(dia == 0 || dia == 7) {
			return true;
		} else {
			return false;
		}	
	}
	
	public static boolean isDiaDeSemana(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		int dia = cal.get(Calendar.DAY_OF_WEEK);
		if(dia != 0 || dia != 7) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Date asDate(String pattern, String fecha){		
		Date dt = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			dt = format.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dt;
	}
	
	public static Calendar asCalendar(String pattern, String fecha){
		Calendar cal = Calendar.getInstance();
		try {
			SimpleDateFormat format = new SimpleDateFormat(pattern);		
			cal.setTime(format.parse(fecha));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}
	
	public static String asString(String pattern, String fecha){
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(fecha);
	}


}
