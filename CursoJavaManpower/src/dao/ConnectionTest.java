package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionTest {
	public static void main(String[] args) {
		try {
			
			//1-Especificar el driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			//2-Crear la conexi�n ---> Es una interfaz
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "sistema", "sistema");
			//3-Creamos y usamos el statement ---> Es una interfaz
			Statement stm = conexion.createStatement();
			//4-Ejecutamos la consulta con el ResultSet ---> Es una interfaz
			ResultSet rs = stm.executeQuery("SELECT alu_id, alu_apellido, alu_nombre, alu_estudios, alu_linkgit FROM alumnos");
			while(rs.next()) {
				System.out.println("apellido = " + rs.getString("alu_apellido"));
				System.out.println(", nombre = " + rs.getString("alu_nombre"));
			}
			rs.close();
			conexion.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
